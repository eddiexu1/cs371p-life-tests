*** Life<FredkinCell> 3x2 ***

Generation = 0, Population = 5.
00
-0
00

Generation = 1, Population = 3.
1-
0-
1-

Generation = 2, Population = 5.
20
-0
20

Generation = 3, Population = 3.
3-
0-
3-

Generation = 4, Population = 5.
40
-0
40

Generation = 5, Population = 3.
5-
0-
5-

Generation = 6, Population = 5.
60
-0
60

Generation = 7, Population = 3.
7-
0-
7-

Generation = 8, Population = 5.
80
-0
80

Generation = 9, Population = 3.
9-
0-
9-

Generation = 10, Population = 5.
+0
-0
+0

Generation = 11, Population = 3.
+-
0-
+-

Generation = 12, Population = 5.
+0
-0
+0

Generation = 13, Population = 3.
+-
0-
+-

Generation = 14, Population = 5.
+0
-0
+0

Generation = 15, Population = 3.
+-
0-
+-

Generation = 16, Population = 5.
+0
-0
+0

Generation = 17, Population = 3.
+-
0-
+-

Generation = 18, Population = 5.
+0
-0
+0

Generation = 19, Population = 3.
+-
0-
+-

Generation = 20, Population = 5.
+0
-0
+0

Generation = 21, Population = 3.
+-
0-
+-

Generation = 22, Population = 5.
+0
-0
+0

Generation = 23, Population = 3.
+-
0-
+-

Generation = 24, Population = 5.
+0
-0
+0

Generation = 25, Population = 3.
+-
0-
+-

Generation = 26, Population = 5.
+0
-0
+0

Generation = 27, Population = 3.
+-
0-
+-

Generation = 28, Population = 5.
+0
-0
+0

Generation = 29, Population = 3.
+-
0-
+-

Generation = 30, Population = 5.
+0
-0
+0

Generation = 31, Population = 3.
+-
0-
+-

Generation = 32, Population = 5.
+0
-0
+0

Generation = 33, Population = 3.
+-
0-
+-

Generation = 34, Population = 5.
+0
-0
+0

*** Life<FredkinCell> 8x8 ***

Generation = 0, Population = 23.
0---00--
-----0--
-------0
00-0-0--
0--0-0-0
00------
0--0-0-0
-000---0

Generation = 10, Population = 34.
2-1-4--1
-11-2-0-
-01-12-0
-533332-
--2--402
552-----
---261-5
121---4-

Generation = 20, Population = 41.
2--98-4-
744-4--4
3-652-02
1763942-
8--93--3
85531--7
457-8-5-
-253--4-

Generation = 30, Population = 26.
---+++5-
96---8-7
---748--
--8----8
-+7--+--
---6-8-+
-5+--+-8
-2+-5---

Generation = 40, Population = 29.
6+--+-65
+7--89--
--+-----
-++-+9-+
++8--+--
+-+--+--
-7-+-+-+
--+9--+-

Generation = 50, Population = 36.
8++-++87
+++----+
-3------
-+-7-+6+
+--+--3-
++-+-+6+
8---+-++
-2-+78++

Generation = 60, Population = 33.
-+++++-8
+----+-+
----8+-8
-+-9++-+
++++----
---+4--+
+9+-+---
92---8++

Generation = 70, Population = 33.
-+++-+--
+++-++0-
+-+--+09
5+---++-
-+-+++--
-++--+9-
+-+--+--
-2-+--+-

*** Life<FredkinCell> 3x8 ***

Generation = 0, Population = 17.
0000000-
00-00-00
0---000-

Generation = 1, Population = 9.
-1-11---
1-0-1-11
1-------

Generation = 2, Population = 17.
--02-000
20-02-22
200-0-00

Generation = 3, Population = 12.
0-1-111-
-----033
-1101---

Generation = 4, Population = 13.
----22--
20-02-44
22--2-00

Generation = 5, Population = 9.
01---3-0
---130--
--1-3---

Generation = 6, Population = 14.
1212-4--
20-24-44
-2-04---

Generation = 7, Population = 13.
-3--2--0
-103-055
-3---000

Generation = 8, Population = 14.
14-----1
22-44-66
24-04-1-

*** Life<FredkinCell> 9x4 ***

Generation = 0, Population = 20.
0--0
-000
0-00
00--
--00
0--0
0-0-
-0--
0-00

Generation = 9, Population = 18.
445-
0--6
6524
-2--
77--
-13-
6---
---5
-5-7

Generation = 18, Population = 20.
--+-
-14+
-+--
3--2
++--
4-64
--43
303+
3--+

Generation = 27, Population = 18.
--+-
0-7+
-+--
625-
++--
71--
--76
--6+
6--+

Generation = 36, Population = 20.
+++-
-1-+
++8+
--82
++--
---4
+---
30-+
-+-+

Generation = 45, Population = 18.
+++-
0--+
++++
-2--
++--
-1+-
+---
---+
-+-+

Generation = 54, Population = 20.
--+-
-1++
-+--
+--2
++--
+-+4
--++
30++
+--+

*** Life<FredkinCell> 5x6 ***

Generation = 0, Population = 1.
------
-0----
------
------
------

Generation = 5, Population = 3.
-----0
----0-
-----0
------
------

*** Life<FredkinCell> 8x10 ***

Generation = 0, Population = 18.
---0---000
-----0----
-----0----
--00------
--0----0--
0---------
0000-0----
0-----0---

Generation = 1, Population = 38.
--0-0001-1
---0010000
--00010---
-0-100-0--
001---0-0-
1--0-0-0--
1--1------
1-00---0--

Generation = 2, Population = 30.
-0-0-11-02
--0----1--
--1----000
-1--1-----
-12000---0
---1--0---
-0--00----
2-1-0-0-0-

Generation = 3, Population = 39.
0-0--22113
--1--1-20-
--200-0--1
020-2--00-
0--111--0-
---200-0-0
-----10-0-
-0--10---0

Generation = 4, Population = 36.
--100--224
00---2---0
0-3--1-00-
-------1--
1-----001-
1-0-110---
-0-10210-0
2-102-----

Generation = 5, Population = 46.
0-2-1-23--
-110-30-01
10-002--1-
--0--0--0-
212-1--120
20-2-21000
1----321--
-021--00-0

Generation = 6, Population = 34.
-030-2-4-4
0-2----2-2
2----3-0--
-21-210---
-2-1-1----
3-0-1-2-1-
200-0--2--
-1-22-----

Generation = 7, Population = 39.
-141---5-5
113---0--3
3----40111
--213-1---
-3---20-2-
4-1222-0-0
-1-1-3----
-223-0-0--

Generation = 8, Population = 29.
--521----6
-2----12--
4--0-5----
0-3-4----0
--2-1--1--
--23-32-1-
-2--0-2--0
2----1--0-

Generation = 9, Population = 33.
--6-22252-
----0-2303
5---0--1-1
1-415--10-
23-12----0
---4-4-0--
------320-
-------0--

Generation = 10, Population = 37.
-1----36--
1-30-3--1-
603--5---2
-252611-10
-422------
40252-21-0
---1--4-10
-------1--

Generation = 11, Population = 37.
---2--47--
2--1-4----
-140----13
-3-3-22-2-
-5-322-12-
513-3--211
22-2-35-2-
---3-----0

Generation = 12, Population = 43.
0-632-582-
-------313
---10-01-4
1-5-6-31--
--2-3-0-3-
--4-44---2
--03046230
2224210---

Generation = 13, Population = 41.
--7-3-69--
2-3---242-
6--2---215
--63--4--0
25---211--
-1-55-2213
2-14--7---
3-3532--00

Generation = 14, Population = 40.
018-------
3-4-04----
71430-0-2-
--7-6-51-1
3----3----
524-6433-4
32-50-8---
4--6-3-111

Generation = 15, Population = 39.
---332----
---1-5--2-
----1512--
--8372-22-
4---341---
6355-544--
4-1---923-
5-373-02-2

Generation = 16, Population = 31.
--8---6-2-
--42-----3
--43----2-
139--3--31
5--3---13-
7-666-----
-2-50-+-4-
6---4---1-

Generation = 17, Population = 42.
-19--2----
-25-0-2-2-
7-5-15-235
---37-5-42
--24-412--
837--544--
4-1-1---50
----530222

Generation = 18, Population = 32.
0--3----2-
--6-1--43-
-16326-3--
1-9---6---
5---3----0
--8---5--4
522-24-2--
6-37----3-

Generation = 19, Population = 38.
-1------36
3-7-2-2--3
-2--3714--
23+------2
65-4-4--31
--96----15
--3535+--0
72-8-3---2

Generation = 20, Population = 41.
----3-694-
----353--4
7363--253-
--+-7-62-3
---5--12-2
8--7--54-6
5----6+2--
833954--33

Generation = 21, Population = 41.
---342--5-
327---443-
8-743-3645
2-+---7344
6--6-423--
93986-65-7
--3---+---
--------44

Generation = 22, Population = 39.
01-4-3--66
---2---54-
-38-4-4---
--+37-8-5-
--2735-4--
--+9757---
5-4-36+-5-
--3---0255

Generation = 23, Population = 39.
12-5--6--7
3--3-5-6--
84--5-5---
--+48393-4
-53-------
93++----1-
---5-7+26-
83495---66

Generation = 24, Population = 46.
-3-64---6-
-2743---44
-5-46-66-5
23--9-+4--
--47352432
+4+-757-27
--4-38--7-
9--+6-0--7

Generation = 25, Population = 41.
149--3-97-
33--454---
--8-7---46
3--4-3+5--
-5---6-54-
+-++8--53-
--554-+280
-3-+-4-2--

Generation = 26, Population = 42.
-5+6-46+87
--74-65--4
---487--57
----9---54
6----726-2
-4-+---64-
5----8+-91
9-4+6-03-7

Generation = 27, Population = 33.
16+---7---
---547--4-
----9--6-8
3---+3+--5
---7-8---3
+---8-7--7
6-5--9----
+-5-74-468

Generation = 28, Population = 39.
2-+-4--+8-
33-658----
8----7--59
-3---4+-5-
--4--92-4-
+4+----648
--654+-29-
+-6-8--5-9

Generation = 29, Population = 44.
36-65-7+97
44776-56-4
95-4---6-+
----+5----
65573--6--
+-+-8----9
--7-5+-3-1
--7+9-066-

Generation = 30, Population = 40.
--+----+--
5------74-
--8597-7-+
--+4-6+--5
--6-4-2-43
+4-+----4+
62-56++492
-3---4177-

Generation = 31, Population = 41.
36-6--7+--
-4-768585-
95-6--685+
-3+-------
6-7--93--4
--++85--5+
-3---++5--
-47+--28--

Generation = 32, Population = 32.
4--7------
5--8------
+687---96+
3----6-55-
-5----46-5
+----67-6-
647---+--2
+--+9---7-

Generation = 33, Population = 46.
56+85-----
----6--854
+---976-7+
43+4+-+6-5
6-7--9----
+4+-8786-+
7-8-6-+59-
+--++428--

Generation = 34, Population = 36.
6----4-+97
-47----96-
+-8-+----+
5-+----756
----4+4---
+-+--89-6-
-495--+-+-
+4-++-3---

Generation = 35, Population = 40.
--+-5---+8
55-8685--4
+6---7--7+
6-+4-6-8--
---75+---5
-4+---+-7+
7-+-----+2
+-7-+-487-

Generation = 36, Population = 35.
6--86-7++-
--7979696-
+-------8-
73+-+-----
657----6-6
-5--8----+
84-56---+-
+-8---5---

Generation = 37, Population = 43.
-6---4-+-8
-58+8-7+74
+--7-76-9+
84---6-856
---7--4--7
+---98-6-+
--+6-++5--
+4--+4-87-

Generation = 38, Population = 46.
67-8--7++9
---+9--+85
+6-8+---++
-5+-+7-967
-57--+-6--
-5+++--7-+
--+7-+-6-2
+5--+-5-89

Generation = 39, Population = 41.
78-9-48++-
--8+-9---6
+-8-+--9++
-6-4----7-
668-5+----
+--++8+--+
84----+7+-
+6---4-89-

*** Life<FredkinCell> 10x6 ***

Generation = 0, Population = 4.
------
0-----
-----0
---0--
------
------
-0----
------
------
------

*** Life<FredkinCell> 3x9 ***

Generation = 0, Population = 17.
0-000-00-
00--00-0-
000-00--0

Generation = 6, Population = 11.
-0-0-----
-0-23--41
1-3--4-0-

*** Life<FredkinCell> 1x10 ***

Generation = 0, Population = 9.
000000-000

Generation = 9, Population = 5.
---2-2230-

Generation = 18, Population = 3.
5--5---5--

Generation = 27, Population = 5.
--9--57-55

Generation = 36, Population = 6.
--+-+7+89-

Generation = 45, Population = 8.
++++++--++

Generation = 54, Population = 5.
---+++-++-

Generation = 63, Population = 4.
+----+-+-+

Generation = 72, Population = 4.
--+--+--++

Generation = 81, Population = 5.
-++-+-+-+-

*** Life<FredkinCell> 1x8 ***

Generation = 0, Population = 7.
00000-00

Generation = 7, Population = 7.
11-33055

*** Life<FredkinCell> 6x9 ***

Generation = 0, Population = 8.
---------
-0-0--0--
--------0
---0-----
---------
--0----00

Generation = 7, Population = 32.
-00-1--0-
012--60-1
-0202-600
0---02--4
-000-0202
-1-21---1

Generation = 14, Population = 18.
--3-1----
----7+-1-
3-----7-3
-2----6-6
2---1----
-27--0-86

Generation = 21, Population = 18.
-0-------
------4-6
---0-0-0-
------+--
-020--506
-4-2-23-+

*** Life<FredkinCell> 5x6 ***

Generation = 0, Population = 2.
------
0-----
------
----0-
------

*** Life<FredkinCell> 2x3 ***

Generation = 0, Population = 6.
000
000

Generation = 7, Population = 2.
-7-
-7-

Generation = 14, Population = 6.
0+0
0+0

Generation = 21, Population = 2.
-+-
-+-

Generation = 28, Population = 6.
0+0
0+0

Generation = 35, Population = 2.
-+-
-+-

*** Life<FredkinCell> 8x8 ***

Generation = 0, Population = 17.
-------0
0--00-0-
0000----
0---00--
--00----
-------0
----0---
0-------

Generation = 6, Population = 40.
54-5-010
--0-20-0
-410-530
2-1----1
--43-13-
1110-001
-22101--
0--0-000

*** Life<FredkinCell> 3x9 ***

Generation = 0, Population = 12.
-0--00--0
00-00-0--
00-0-----

Generation = 6, Population = 14.
-1222-3-0
0----523-
-0--20--2

Generation = 12, Population = 14.
-5--45--0
-535----3
-0334-2-4

Generation = 18, Population = 14.
-6346-+-2
2----+68-
-0--60--6

Generation = 24, Population = 14.
-+--8+--2
-+7+----5
-0668-5-8

Generation = 30, Population = 14.
-+46+-+-4
4----+++-
-0--+0--+

Generation = 36, Population = 14.
-+--++--4
-+++----7
-099+-8-+

Generation = 42, Population = 14.
-+58+-+-6
6----+++-
-0--+0--+

Generation = 48, Population = 14.
-+--++--6
-+++----9
-0+++-+-+

Generation = 54, Population = 14.
-+6++-+-8
8----+++-
-0--+0--+

Generation = 60, Population = 14.
-+--++--8
-+++----+
-0+++-+-+

Generation = 66, Population = 14.
-+7++-+-+
+----+++-
-0--+0--+

Generation = 72, Population = 14.
-+--++--+
-+++----+
-0+++-+-+

Generation = 78, Population = 14.
-+8++-+-+
+----+++-
-0--+0--+

Generation = 84, Population = 14.
-+--++--+
-+++----+
-0+++-+-+

Generation = 90, Population = 14.
-+9++-+-+
+----+++-
-0--+0--+

*** Life<FredkinCell> 10x2 ***

Generation = 0, Population = 11.
0-
-0
-0
0-
0-
0-
00
00
--
0-

Generation = 6, Population = 10.
--
23
-3
2-
0-
10
--
-4
--
03

Generation = 12, Population = 8.
-5
--
--
43
0-
--
-2
2-
22
--

*** Life<FredkinCell> 8x8 ***

Generation = 0, Population = 15.
00--0---
0-----0-
00------
0--0----
-0-000--
----0---
-----0--
--------

Generation = 3, Population = 28.
-1-----0
00-1--0-
----01--
-22-12--
10--1-21
10-1--10
-----10-
-0-01--0

Generation = 6, Population = 27.
----10--
3---1--0
---1-1--
--5-42-0
-----32-
2-012--0
21010---
-1-340--

Generation = 9, Population = 31.
031-2--2
-011-0-0
2-22-4-1
2251---0
--1--4--
30---0--
--1--2--
---351-0

Generation = 12, Population = 28.
1--2214-
3--25---
51-----2
-3--8--1
2--1---2
----4--1
43----0-
2-14612-

Generation = 15, Population = 29.
45--3154
-01---32
--------
-4---631
32----52
------22
-421-21-
5--46--0

Generation = 18, Population = 19.
-612----
4-------
5-5-----
--8-+--1
-3--45--
------5-
4----5-1
5--59---

Generation = 21, Population = 34.
5--3-264
-0--613-
6----82-
-6-1+94-
5--14-84
73-5----
-73-3--1
-53-+22-

Generation = 24, Population = 30.
68-3--77
6---72--
7-78-+--
----+-52
-35-4985
840-5-5-
6-----11
---8----

Generation = 27, Population = 31.
792-7-+8
--3---42
--89--36
48+--+5-
--5-5-8-
-5---0-4
-9-2--2-
855----0

Generation = 30, Population = 27.
+--38-+-
--449---
--9--+--
-9+-++8-
8-6---+6
8-06--6-
-+--46-2
----+---

Generation = 33, Population = 32.
-+-48---
8--5----
+-++0--6
5-+-+-8-
9--2-+-7
96--70--
9+-2--2-
+66++--0

Generation = 36, Population = 33.
--2-9-+9
9-4-+-7-
-1-+0+56
5---++9-
+563-+-8
--098-8-
-+-36---
+-----5-

Generation = 39, Population = 31.
-+----++
--55+--4
+-++-+-7
6+--+---
----7---
-8--90-4
+-5368-3
+-6-+-50

Generation = 42, Population = 28.
+-------
9---+385
+-++0-6-
7-+--++-
+59---+-
+9-9----
--5-6--3
+99-----

Generation = 45, Population = 28.
-+-----+
90-7--9-
----0+--
-++-++--
+6--7-++
+9-+--+6
-----+3-
-9-++--0

Generation = 48, Population = 27.
----+3--
+---+--6
---+-+--
--+-++-3
-----++-
+-0++--6
++646---
-+-++3--

Generation = 51, Population = 31.
++4-+--+
-077-3-6
+-++-+-+
8++1---3
--+--+--
+9---0--
--7--+--
---++4-0

Generation = 54, Population = 28.
+--5+4+-
+--8+---
+1-----+
-+--+--4
+--4---+
----+--7
++----3-
+-+++48-

Generation = 57, Population = 29.
++--+4++
-07---+8
--------
-+---++4
+8----++
------+8
-+84-+4-
+--++--0

Generation = 60, Population = 19.
-+45----
+-------
+-+-----
--+-+--4
-9--++--
------+-
+----+-4
+--++---

Generation = 63, Population = 34.
+--6-5++
-0--+4+-
+----+8-
-+-1+++-
+--4+-++
++-+----
-+9-9--4
-++-+58-

Generation = 66, Population = 30.
++-6--++
+---+5--
+-++-+--
----+-+5
-9+-++++
++0-+-+-
+-----44
---+----

Generation = 69, Population = 31.
++5-+-++
--9---+8
--++--9+
+++--++-
--+-+-+-
-+---0-+
-+-5--5-
+++----0

Generation = 72, Population = 27.
+--6+-+-
--+++---
--+--+--
-++-+++-
+-+---++
+-0+--+-
-+--++-5
----+---

Generation = 75, Population = 32.
-+-7+---
+--+----
+-++0--+
+-+-+-+-
+--5-+-+
++--+0--
++-5--5-
+++++--0

Generation = 78, Population = 33.
--5-+-++
+-+-+-+-
-1-+0+++
+---+++-
+++6-+-+
--0++-+-
-+-6+---
+-----+-

Generation = 81, Population = 31.
-+----++
--+++--+
+-++-+-+
++--+---
----+---
-+--+0-+
+-+6++-6
+-+-+-+0

Generation = 84, Population = 28.
+-------
+---+6++
+-++0-+-
+-+--++-
+++---+-
++-+----
--+-+--6
+++-----

*** Life<FredkinCell> 3x3 ***

Generation = 0, Population = 5.
-00
-0-
0-0

*** Life<FredkinCell> 5x1 ***

Generation = 0, Population = 5.
0
0
0
0
0

Generation = 3, Population = 2.
1
-
-
-
1

Generation = 6, Population = 2.
-
0
-
0
-

Generation = 9, Population = 2.
1
-
-
-
1

Generation = 12, Population = 2.
-
0
-
0
-

Generation = 15, Population = 2.
1
-
-
-
1

Generation = 18, Population = 2.
-
0
-
0
-

Generation = 21, Population = 2.
1
-
-
-
1

Generation = 24, Population = 2.
-
0
-
0
-

Generation = 27, Population = 2.
1
-
-
-
1

Generation = 30, Population = 2.
-
0
-
0
-

Generation = 33, Population = 2.
1
-
-
-
1

Generation = 36, Population = 2.
-
0
-
0
-

Generation = 39, Population = 2.
1
-
-
-
1

Generation = 42, Population = 2.
-
0
-
0
-

Generation = 45, Population = 2.
1
-
-
-
1

Generation = 48, Population = 2.
-
0
-
0
-

Generation = 51, Population = 2.
1
-
-
-
1

Generation = 54, Population = 2.
-
0
-
0
-

Generation = 57, Population = 2.
1
-
-
-
1

Generation = 60, Population = 2.
-
0
-
0
-

Generation = 63, Population = 2.
1
-
-
-
1

*** Life<FredkinCell> 1x8 ***

Generation = 0, Population = 3.
-0--00--

Generation = 2, Population = 3.
--1---10

Generation = 4, Population = 3.
0----2-2

Generation = 6, Population = 4.
0-10-2--

Generation = 8, Population = 6.
-03212-2

Generation = 10, Population = 5.
123-1-2-

Generation = 12, Population = 2.
---2--2-

Generation = 14, Population = 3.
-2--24--

Generation = 16, Population = 3.
--5---32

Generation = 18, Population = 3.
2----6-4

Generation = 20, Population = 4.
2-52-6--

Generation = 22, Population = 6.
-27436-4

Generation = 24, Population = 5.
347-3-4-

Generation = 26, Population = 2.
---4--4-

Generation = 28, Population = 3.
-4--48--

Generation = 30, Population = 3.
--9---54

Generation = 32, Population = 3.
4----+-6

Generation = 34, Population = 4.
4-94-+--

Generation = 36, Population = 6.
-4+65+-6

Generation = 38, Population = 5.
56+-5-6-

Generation = 40, Population = 2.
---6--6-

Generation = 42, Population = 3.
-6--6+--

Generation = 44, Population = 3.
--+---76

Generation = 46, Population = 3.
6----+-8

Generation = 48, Population = 4.
6-+6-+--

Generation = 50, Population = 6.
-6+87+-8

Generation = 52, Population = 5.
78+-7-8-

Generation = 54, Population = 2.
---8--8-

Generation = 56, Population = 3.
-8--8+--

Generation = 58, Population = 3.
--+---98

Generation = 60, Population = 3.
8----+-+

Generation = 62, Population = 4.
8-+8-+--

Generation = 64, Population = 6.
-8++9+-+

Generation = 66, Population = 5.
9++-9-+-

Generation = 68, Population = 2.
---+--+-

Generation = 70, Population = 3.
-+--++--

Generation = 72, Population = 3.
--+---++

Generation = 74, Population = 3.
+----+-+

Generation = 76, Population = 4.
+-++-+--

Generation = 78, Population = 6.
-+++++-+

Generation = 80, Population = 5.
+++-+-+-

Generation = 82, Population = 2.
---+--+-

Generation = 84, Population = 3.
-+--++--

Generation = 86, Population = 3.
--+---++

Generation = 88, Population = 3.
+----+-+

Generation = 90, Population = 4.
+-++-+--

*** Life<FredkinCell> 8x1 ***

Generation = 0, Population = 8.
0
0
0
0
0
0
0
0

Generation = 4, Population = 2.
-
-
-
0
0
-
-
-

Generation = 8, Population = 2.
2
-
-
-
-
-
-
2

Generation = 12, Population = 4.
-
-
2
2
2
2
-
-

Generation = 16, Population = 2.
-
2
-
-
-
-
2
-

Generation = 20, Population = 4.
-
2
5
-
-
5
2
-

Generation = 24, Population = 4.
4
-
6
-
-
6
-
4

Generation = 28, Population = 8.
4
4
8
4
4
8
4
4

Generation = 32, Population = 2.
-
-
-
4
4
-
-
-

Generation = 36, Population = 2.
6
-
-
-
-
-
-
6

Generation = 40, Population = 4.
-
-
+
6
6
+
-
-

Generation = 44, Population = 2.
-
6
-
-
-
-
6
-

Generation = 48, Population = 4.
-
6
+
-
-
+
6
-

Generation = 52, Population = 4.
8
-
+
-
-
+
-
8

Generation = 56, Population = 8.
8
8
+
8
8
+
8
8

Generation = 60, Population = 2.
-
-
-
8
8
-
-
-

Generation = 64, Population = 2.
+
-
-
-
-
-
-
+

Generation = 68, Population = 4.
-
-
+
+
+
+
-
-

*** Life<FredkinCell> 8x6 ***

Generation = 0, Population = 9.
---000
-0----
----0-
-0--0-
------
----0-
------
--0---

Generation = 1, Population = 23.
-001-1
0-00-0
---010
0-0010
-0----
---0-0
--0-0-
-0-0--

Generation = 2, Population = 19.
-11--2
-0-10-
--0121
-0112-
----0-
-0--0-
------
0-0---

Generation = 3, Population = 24.
0-2---
01---0
-0---2
01223-
--0--0
0-0010
000-0-
---0--

Generation = 4, Population = 24.
11-1-2
----01
0--1-3
123-40
---001
101-2-
----1-
00----

Generation = 5, Population = 24.
22--03
-1-112
1-----
--4--1
--01--
2-20--
--0020
110-0-

Generation = 6, Population = 28.
3-2---
02-22-
--012-
125-4-
001-01
--31-0
--1131
2--01-

Generation = 7, Population = 21.
42--0-
-3---2
-0-233
-362--
-121--
2-4---
00--4-
-1----

Generation = 8, Population = 23.
5-21--
--0223
1-0---
1473-1
----0-
3-5-2-
---1-1
-20-1-

Generation = 9, Population = 23.
-----3
-313-4
20-23-
--844-
-1--1-
---13-
001---
2310--

Generation = 10, Population = 28.
-22104
----25
3--3--
149551
0--1-1
3--2-0
-1-14-
-4211-

Generation = 11, Population = 22.
53--1-
0313--
4--43-
2-----
----1-
4-5-31
-212--
2--2-0

Generation = 12, Population = 20.
-----4
14--2-
----43
34-5--
--212-
-062-2
032---
---3--

Generation = 13, Population = 26.
53----
251335
-----4
459651
01--3-
----3-
14-2-1
24--1-

Generation = 14, Population = 9.
---114
-6----
----4-
-6--6-
------
----4-
------
--2---

Generation = 15, Population = 23.
-322-5
2-13-5
---454
4-9671
-1----
---2-2
--2-4-
-4-3--

Generation = 16, Population = 19.
-43--6
-6-43-
--0565
-6+78-
----3-
-0--4-
------
2-2---

Generation = 17, Population = 24.
5-4---
27---5
-0---6
47+89-
--2--1
4-6252
142-4-
---3--

Generation = 18, Population = 24.
64-2-6
----36
4--5-7
58+-+1
---132
507-6-
----5-
24----

Generation = 19, Population = 24.
75--17
-7-447
5-----
--+--2
--22--
6-82--
--2261
352-1-

Generation = 20, Population = 28.
8-4---
28-55-
--056-
58+-+-
013-32
--93-2
--3372
4--32-

Generation = 21, Population = 21.
95--1-
-9---7
-0-677
-9+8--
-242--
6-+---
14--8-
-5----

Generation = 22, Population = 23.
+-42--
--1558
5-0---
5++9-2
----3-
7-+-6-
---3-2
-62-2-

Generation = 23, Population = 23.
-----7
-926-9
60-67-
--+++-
-2--4-
---37-
143---
4733--

Generation = 24, Population = 28.
-54218
----5+
7--7--
5++++2
0--2-2
7--4-2
-5-38-
-8442-

Generation = 25, Population = 22.
+6--2-
2926--
8--87-
6-----
----4-
8-+-73
-634--
4--5-0

Generation = 26, Population = 20.
-----8
3+--5-
----87
7+-+--
--425-
-0+4-4
174---
---6--

Generation = 27, Population = 26.
+6----
4+266+
-----8
8++++2
02--6-
----7-
28-4-2
48--2-

Generation = 28, Population = 9.
---228
-+----
----8-
-+--+-
------
----8-
------
--4---

Generation = 29, Population = 23.
-643-9
4-26-+
---898
8-+++2
-2----
---4-4
--4-8-
-8-6--

Generation = 30, Population = 19.
-75--+
-+-76-
--09+9
-++++-
----6-
-0--8-
------
4-4---

Generation = 31, Population = 24.
+-6---
4+---+
-0---+
8++++-
--4--2
8-+494
284-8-
---6--

Generation = 32, Population = 24.
+7-3-+
----6+
8--9-+
9++-+2
---263
90+-+-
----9-
48----

Generation = 33, Population = 24.
+8--2+
-+-77+
9-----
--+--3
--43--
+-+4--
--44+2
594-2-

Generation = 34, Population = 28.
+-6---
4+-88-
--09+-
9++-+-
025-63
--+5-4
--55+3
6--63-

Generation = 35, Population = 21.
+8--2-
-+---+
-0-+++
-+++--
-363--
+-+---
28--+-
-9----

Generation = 36, Population = 23.
+-63--
--288+
9-0---
9+++-3
----6-
+-+-+-
---5-3
-+4-3-

Generation = 37, Population = 23.
-----+
-+39-+
+0-++-
--+++-
-3--7-
---5+-
285---
6+56--

Generation = 38, Population = 28.
-8632+
----8+
+--+--
9++++3
0--3-3
+--6-4
-9-5+-
-+673-

Generation = 39, Population = 22.
+9--3-
4+39--
+--++-
+-----
----7-
+-+-+5
-+56--
6--8-0

Generation = 40, Population = 20.
-----+
5+--8-
----++
++-+--
--638-
-0+6-6
2+6---
---9--

Generation = 41, Population = 26.
+9----
6+399+
-----+
+++++3
03--9-
----+-
3+-6-3
6+--3-

Generation = 42, Population = 9.
---33+
-+----
----+-
-+--+-
------
----+-
------
--6---

Generation = 43, Population = 23.
-964-+
6-39-+
---+++
+-+++3
-3----
---6-6
--6-+-
-+-9--

Generation = 44, Population = 19.
-+7--+
-+-+9-
--0+++
-++++-
----9-
-0--+-
------
6-6---

Generation = 45, Population = 24.
+-8---
6+---+
-0---+
+++++-
--6--3
+-+6+6
3+6-+-
---9--

Generation = 46, Population = 24.
++-4-+
----9+
+--+-+
+++-+3
---394
+0+-+-
----+-
6+----

Generation = 47, Population = 24.
++--3+
-+-+++
+-----
--+--4
--64--
+-+6--
--66+3
7+6-3-

Generation = 48, Population = 28.
+-8---
6+-++-
--0++-
+++-+-
037-94
--+7-6
--77+4
8--94-

Generation = 49, Population = 21.
++--3-
-+---+
-0-+++
-+++--
-484--
+-+---
3+--+-
-+----

Generation = 50, Population = 23.
+-84--
--3+++
+-0---
++++-4
----9-
+-+-+-
---7-4
-+6-4-

Generation = 51, Population = 23.
-----+
-+4+-+
+0-++-
--+++-
-4--+-
---7+-
3+7---
8+79--

Generation = 52, Population = 28.
-+843+
----++
+--+--
+++++4
0--4-4
+--8-6
-+-7+-
-+8+4-

Generation = 53, Population = 22.
++--4-
6+4+--
+--++-
+-----
----+-
+-+-+7
-+78--
8--+-0

Generation = 54, Population = 20.
-----+
7+--+-
----++
++-+--
--84+-
-0+8-8
3+8---
---+--

Generation = 55, Population = 26.
++----
8+4+++
-----+
+++++4
04--+-
----+-
4+-8-4
8+--4-

Generation = 56, Population = 9.
---44+
-+----
----+-
-+--+-
------
----+-
------
--8---

Generation = 57, Population = 23.
-+85-+
8-4+-+
---+++
+-+++4
-4----
---8-8
--8-+-
-+-+--

Generation = 58, Population = 19.
-+9--+
-+-++-
--0+++
-++++-
----+-
-0--+-
------
8-8---

Generation = 59, Population = 24.
+-+---
8+---+
-0---+
+++++-
--8--4
+-+8+8
4+8-+-
---+--

Generation = 60, Population = 24.
++-5-+
----++
+--+-+
+++-+4
---4+5
+0+-+-
----+-
8+----

Generation = 61, Population = 24.
++--4+
-+-+++
+-----
--+--5
--85--
+-+8--
--88+4
9+8-4-

Generation = 62, Population = 28.
+-+---
8+-++-
--0++-
+++-+-
049-+5
--+9-8
--99+5
+--+5-

Generation = 63, Population = 21.
++--4-
-+---+
-0-+++
-+++--
-5+5--
+-+---
4+--+-
-+----

*** Life<FredkinCell> 7x6 ***

Generation = 0, Population = 19.
------
00--0-
-0-0--
00--00
----00
-0-00-
00-000

Generation = 3, Population = 19.
02-1-0
1--112
01-1-0
10----
2-----
1----0
1--1--

Generation = 6, Population = 21.
1-1---
-00413
3---1-
-0-0-0
-11--2
13----
422--3

Generation = 9, Population = 24.
1-2201
-13---
330-2-
--3---
3-4-2-
2621-1
535-3-

Generation = 12, Population = 21.
1-2--4
--6-45
4-133-
--32--
-3----
392--1
56-3-3

Generation = 15, Population = 30.
14-205
41-4-6
5---44
32-213
46-235
--3344
57--36

Generation = 18, Population = 19.
-4-3-5
-17---
-436-7
-2-2--
-6--36
-9--47
-8---7

Generation = 21, Population = 19.
--630-
-4---6
-7474-
----2-
--5-37
3+--4-
-96-4-

Generation = 24, Population = 15.
------
-5-66-
-----7
-2--3-
--636-
--6-59
---47+

Generation = 27, Population = 19.
--83-6
-5--97
--57-8
-344--
--7---
--97--
-+87-+

Generation = 30, Population = 18.
2---4-
6--+-+
----7-
---4-4
677-8-
-++8--
9+--9-

Generation = 33, Population = 26.
369--7
979++-
7957--
7-5-4-
9-7---
7--99+
+++8--

Generation = 36, Population = 24.
3--65-
+--+--
8+68-+
766456
-9-3--
7+--+-
--+8-+

Generation = 39, Population = 19.
---687
--9+-+
---98-
---556
---68+
8+---+
-+--+-

Generation = 42, Population = 20.
---7-8
+9+++-
+-9+8+
-----6
-9+-9-
-++-+-
-+----

Generation = 45, Population = 17.
79-+-8
-+----
+----+
--7-5-
+--6--
-+-++-
++-+--

Generation = 48, Population = 18.
------
--+-++
---+--
-6885-
+9-9-+
+--+--
-++-++

Generation = 51, Population = 31.
99+-98
-++-+-
+++-9+
8-8-68
+-+-+-
++++++
+-++++

Generation = 54, Population = 19.
----+-
-++--+
--+-+-
-6---8
++++--
+-+-++
-+-++-

Generation = 57, Population = 19.
++--+-
+-+--+
+---++
9-8---
+---+-
-+--++
+---++

Generation = 60, Population = 25.
-+-+++
-+++-+
+--+++
+-88-8
-----+
-+-++-
+++-++

Generation = 63, Population = 28.
-++-++
+++-++
-+++++
-99-89
-++-++
-++---
-++++-

Generation = 66, Population = 18.
-+---+
+-+--+
+++--+
---8--
++-+--
++-+--
-+-+--

Generation = 69, Population = 17.
--++-+
-+-+--
--++--
+-----
-+++-+
--+-++
--+--+

Generation = 72, Population = 16.
------
+--+--
-+-+-+
+----+
--++-+
-+++-+
++----

*** Life<FredkinCell> 10x4 ***

Generation = 0, Population = 4.
0---
----
----
----
0---
----
0---
0---
----
----

Generation = 8, Population = 9.
--0-
----
0---
----
2-0-
3---
3-6-
3-4-
----
----

Generation = 16, Population = 9.
--0-
3---
----
----
6-1-
----
4-7-
7-+-
6---
----

Generation = 24, Population = 14.
8-1-
5-3-
4-1-
----
8-3-
--3-
6-8-
--+-
--3-
6---

Generation = 32, Population = 11.
9-6-
6---
6---
6---
+-9-
----
--+-
9-+-
+---
----

Generation = 40, Population = 9.
+---
7-6-
----
--5-
--+-
--5-
----
--+-
+-+-
----

Generation = 48, Population = 9.
----
----
--+-
7-6-
--+-
----
+---
+-+-
+---
+---

Generation = 56, Population = 9.
----
+---
+-+-
----
--+-
+-7-
--+-
--+-
+---
----

Generation = 64, Population = 10.
+-+-
----
----
----
+-+-
+---
+-+-
--+-
+---
+---

Generation = 72, Population = 11.
----
----
+-+-
+---
+-+-
+---
--+-
----
+-+-
+-9-

Generation = 80, Population = 13.
+---
--+-
+---
+---
--+-
+-+-
+-+-
+---
+---
+-+-

*** Life<FredkinCell> 2x5 ***

Generation = 0, Population = 9.
00000
-0000

Generation = 6, Population = 3.
4-2-2
-----

Generation = 12, Population = 3.
4-2-2
-----

*** Life<FredkinCell> 6x9 ***

Generation = 0, Population = 3.
-----0---
---------
----0----
---------
---------
---0-----

Generation = 9, Population = 22.
2-121-1--
-1---10--
-1-0---2-
--0--00--
----1102-
--0--20-0

Generation = 18, Population = 27.
455-6-310
---1-----
2641-5632
-3-1-1-2-
1-1--41-2
-0-----3-

Generation = 27, Population = 20.
--937--1-
0---0----
-+7346---
-6----0--
44--9--4-
-003-----

*** Life<FredkinCell> 6x7 ***

Generation = 0, Population = 24.
-00--0-
00--0-0
--00000
-0-0--0
----0-0
0000000

Generation = 3, Population = 21.
-----01
3-003--
20-33--
-12-20-
0-0--2-
3-10--0

Generation = 6, Population = 18.
21--0--
403--0-
-1-4---
2----0-
3-1-1--
60--40-

Generation = 9, Population = 20.
2-1112-
--61---
-2---12
---3--0
33--4-1
7-1-5-2

Generation = 12, Population = 22.
31---5-
----4-5
23--64-
-22----
4-12742
+223--3

Generation = 15, Population = 19.
-2--25-
516--0-
5-3--5-
5--3---
75258--
--5---4

Generation = 18, Population = 17.
-5--5--
-27-7-5
63---8-
--4---1
8--8+-4
+-----4

Generation = 21, Population = 17.
7-34---
6-8----
--6--8-
5--44-4
-549-4-
---39--

Generation = 24, Population = 20.
--47964
9--38--
-5-6---
-36--14
--5+-4-
-4-3--6

Generation = 27, Population = 26.
+6-++--
+3-4-0-
-8798-5
-46--15
-75++7-
-59-+1-

Generation = 30, Population = 17.
-7-+---
-48----
--8+---
5-9-81-
+85++--
+----1-

Generation = 33, Population = 14.
-8---76
--9---5
--++---
8---9--
+---++-
---6--6

Generation = 36, Population = 22.
+9--++-
-4+-9-8
7-++-+8
+--6+--
-88+--5
-69----

Generation = 39, Population = 18.
+-7--+-
-4+5-0-
---+---
-7-7-46
-9-+++-
+--6---

Generation = 42, Population = 19.
+9---+-
-5--+--
---+-+8
+8--+-9
-+-+++6
--+---7

Generation = 45, Population = 17.
--+--+-
--+6+-+
+++----
--+---+
-+-+---
+--7+-+

Generation = 48, Population = 24.
-+-+---
+8+6+3+
-+-++-8
+-+-+4-
-++--+-
-7+--2+

*** Life<FredkinCell> 6x10 ***

Generation = 0, Population = 11.
0-0--0-0--
---00----0
----------
-------0--
--00-0----
----------

Generation = 7, Population = 29.
-321-1-0-2
2-1-2-102-
---2-10--0
2-041-----
--2--002--
---14--201

Generation = 14, Population = 35.
-474438---
514-34-1--
-1-5-322-2
43-66-2---
4-7--1121-
-23-6-1-05

Generation = 21, Population = 29.
--+7-8-74-
--7-5--64+
6-2-1--4-2
-6-893----
5-+242---3
---5-32--7

Generation = 28, Population = 34.
5-+--+--75
-37+--2-7+
72---7+-29
8819-366--
-7-3--68--
46--8344--

Generation = 35, Population = 36.
---++++--6
+59+--49--
86475++94+
881++-86-5
--+-9---5-
-6--+38---

Generation = 42, Population = 23.
-------99-
+---8---+-
-6799--+--
+-1+-----8
+-+----+64
96+---+---

Generation = 49, Population = 37.
8---+-++--
-++++++--+
+-8+----4+
+-2--8+87+
++-++++-7-
+7-++-+58-

Generation = 56, Population = 27.
9--+-++-+-
---+++--++
-8++--+-7-
-+5-+8----
-+-+----9-
+-+---+59-

Generation = 63, Population = 31.
--+-+--++-
-+--+++-+-
+8++-+-+9-
++8-+-+9-+
-+--+-+++-
--+----6+-

Generation = 70, Population = 34.
++----+-++
----+-++++
-+++-+-++-
+-8+-+++9-
---+++--+9
-9++-++-+-

Generation = 77, Population = 32.
++----+-++
+-+--+--++
-++--++-+-
++9---+-++
--+++-++++
+--+-++---

*** Life<FredkinCell> 8x10 ***

Generation = 0, Population = 6.
-----0-0--
------0---
----------
----------
-----0----
---------0
--0-------
----------

Generation = 5, Population = 36.
0-0------0
--1--0--0-
-0100-0-00
-00------0
0-1-0103--
000-0-2-00
---0--2--0
0-0---0-0-

Generation = 10, Population = 39.
03--10---1
2-1-0--12-
2-213--0--
01-0---2--
--2-12---1
-3-2123-10
-210--2--2
-0-01---0-

Generation = 15, Population = 41.
0-5---1--1
-2-1---433
5-2-4--22-
-2-1-525--
--3-323--2
--1434--23
-4-362-23-
0---20-01-

Generation = 20, Population = 34.
--5-------
4-42-1----
-222----3-
--2--6-5-4
35----38-5
25-----32-
153-9445-4
---2--52-1

Generation = 25, Population = 42.
277--2-42-
-4----2457
-3-35-24-6
5--2--37--
36-45--+3+
--17--7644
-8--+--75-
-3--31-2--

Generation = 30, Population = 48.
-8+42-1522
-45471268-
56--8-4--9
85--5-384-
39---5-+--
763---87-4
-+53+56---
3-4833----

Generation = 35, Population = 42.
-9+6--17-3
6-+-7138--
--34-3-7--
-77-7639--
4+-566--7+
763-6--77-
--56-6-+--
--49-3----

Generation = 40, Population = 37.
--+--42-63
6-+6+---++
-957865-6-
-7----4---
--6------+
8-5-9+----
-+6--9++--
-7494-7--7

Generation = 45, Population = 39.
----4----3
6-+---5+-+
6+---86-9+
9875+9--6+
6-8--7-+-+
----+-+-7+
--9-++----
5779----67

Generation = 50, Population = 45.
+---4--975
8-++-6-+-+
7-+899-++-
---6++6-7-
9-+86--+--
++-+++-+--
8++-+++---
7---5-85-+

Generation = 55, Population = 45.
+-+-48-+--
97----8-+-
8+-+-+6++-
-+-9--8+9+
++-+9+--+-
+-7-+--++-
-+--+--+89
9+--8+9-8+

Generation = 60, Population = 35.
++++-+---5
+8--+-----
------6++-
++--++9---
+--+++--+-
--8-++---+
+++-+---8-
+---9+--8-

